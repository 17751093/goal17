function viewh2h(f,g)
{
	var x = "";
	var y = "";
	var h1home = "";
	var h2home = "";
	
	$.ajax({
	headers: { 'X-Auth-Token': 'e03efd7c7860409290d19f33137385ef' },
	url: 'http://api.football-data.org/v1/competitions/424/fixtures',
	dataType: 'json',
	type: 'GET',
	}).done(function(response) {
									
		x = f.value;
		y = g.value;
		
		var alertObj = true;
		
		for(var j = 0; j  < response.fixtures.length; j++)
		{
			/*var sample = response.fixtures[0]._links;
			
			var sample9 = sample.self;
			
			
			$('#h2hdisplay').text(sample9.href);*/
			
			h1home = response.fixtures[j].homeTeamName;
			h2away = response.fixtures[j].awayTeamName;
			
			
			if((x == h1home && y == h2away)||(y == h1home && x == h2away))
			{
				
				var linkObj = response.fixtures[j]._links;
				var selfObj = linkObj.self;
				var fixtureUrl = selfObj.href;
							
				$.ajax({
				headers: { 'X-Auth-Token': 'e03efd7c7860409290d19f33137385ef' },
				url: fixtureUrl,
				dataType: 'json',
				type: 'GET',
				}).done(function(response) {
					
					$('#h2hallresult tr').not(function(){ return !!$(this).has('th').length; }).remove();
					
					var h2hobj = response.head2head;
							
					for(var i = 0; i < h2hobj.fixtures.length; i++) {
					
						var date = h2hobj.fixtures[i].date;
						
						var result = h2hobj.fixtures[i].result;
				
						var firstHalfScoreHome = result.goalsHomeTeam;
						var firstHalfScoreAway = result.goalsAwayTeam;
				
						if(result.halftime == null)
						{
							tr = $('<tr/>');
							tr.append("<td>" + date + "</td>");
							tr.append("<td>" + h2hobj.fixtures[i].homeTeamName + "</td>");			
							tr.append("<td>" + firstHalfScoreHome + " - " + firstHalfScoreAway + "</td>");
							tr.append("<td>" + h2hobj.fixtures[i].awayTeamName + "</td>");
							$('#h2hallresult').append(tr);
						}
						else
						{
							var secondResult = result.halfTime;
				
							var secondHalfScoreHome = secondResult.goalsHomeTeam;
							var secondHalfScoreAway = secondResult.goalsAwayTeam;
				
							if(result.extraTime == null)
							{
								var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome;
								var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway;
					
								tr = $('<tr/>');
								tr.append("<td>" + date + "</td>");
								tr.append("<td>" + h2hobj.fixtures[i].homeTeamName + "</td>");			
								tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
								tr.append("<td>" + h2hobj.fixtures[i].awayTeamName + "</td>");
								$('#h2hallresult').append(tr);
							}		
							else
							{
								var extraResult = result.extraTime;
					
								var extraScoreHome = extraResult.goalsHomeTeam;
								var extraScoreAway = extraResult.goalsAwayTeam;
					
								var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome + extraScoreHome;
								var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway + extraScoreAway;
					
								tr = $('<tr/>');
								tr.append("<td>" + date + "</td>");
								tr.append("<td>" + h2hobj.fixtures[i].homeTeamName + "</td>");			
								tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
								tr.append("<td>" + h2hobj.fixtures[i].awayTeamName + "</td>");
								$('#h2hallresult').append(tr);
							}
						}
					}			
				})
				
				alertObj = false;
				break;
			}
			
		}
		if(alertObj == true)
		{
			alert("Sorry! There are no records of your selected teams. Please select another teams.")
		}

	})
}