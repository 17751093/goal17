$.ajax({
 headers: { 'X-Auth-Token': 'e03efd7c7860409290d19f33137385ef' },
 url: 'http://api.football-data.org/v1/competitions/466/leagueTable',
 dataType: 'json',
 type: 'GET',
}).done(function(response) {

	var obj = response.standings;
	
	var tr;
	for(var i = 0; i < 4; i++)
	{
		tr = $('<tr/>');
		tr.append("<td>" + obj.A[i].team + "</td>") ;
		tr.append("<td>" + obj.A[i].points + "</td>");
		tr.append("<td>" + obj.A[i].goals + "</td>");
		tr.append("<td>" + obj.A[i].goalsAgainst + "</td>");
		tr.append("<td>" + obj.A[i].goalDifference + "</td>");
		$('#tableA').append(tr);
	}
	
	for(i = 0; i < 4; i++)
	{
		tr = $('<tr/>');
		tr.append("<td>" + obj.B[i].team + "</td>") ;
		tr.append("<td>" + obj.B[i].points + "</td>");
		tr.append("<td>" + obj.B[i].goals + "</td>");
		tr.append("<td>" + obj.B[i].goalsAgainst + "</td>");
		tr.append("<td>" + obj.B[i].goalDifference + "</td>");
		$('#tableB').append(tr);
	}
	
	for(i = 0; i < 4; i++)
	{
		tr = $('<tr/>');
		tr.append("<td>" + obj.C[i].team + "</td>") ;
		tr.append("<td>" + obj.C[i].points + "</td>");
		tr.append("<td>" + obj.C[i].goals + "</td>");
		tr.append("<td>" + obj.C[i].goalsAgainst + "</td>");
		tr.append("<td>" + obj.C[i].goalDifference + "</td>");
		$('#tableC').append(tr);
	}
	
	for(i = 0; i < 4; i++)
	{
		tr = $('<tr/>');
		tr.append("<td>" + obj.D[i].team + "</td>") ;
		tr.append("<td>" + obj.D[i].points + "</td>");
		tr.append("<td>" + obj.D[i].goals + "</td>");
		tr.append("<td>" + obj.D[i].goalsAgainst + "</td>");
		tr.append("<td>" + obj.D[i].goalDifference + "</td>");
		$('#tableD').append(tr);
	}
	
	for(i = 0; i < 4; i++)
	{
		tr = $('<tr/>');
		tr.append("<td>" + obj.E[i].team + "</td>") ;
		tr.append("<td>" + obj.E[i].points + "</td>");
		tr.append("<td>" + obj.E[i].goals + "</td>");
		tr.append("<td>" + obj.E[i].goalsAgainst + "</td>");
		tr.append("<td>" + obj.E[i].goalDifference + "</td>");
		$('#tableE').append(tr);
	}
	
	for(i = 0; i < 4; i++)
	{
		tr = $('<tr/>');
		tr.append("<td>" + obj.F[i].team + "</td>") ;
		tr.append("<td>" + obj.F[i].points + "</td>");
		tr.append("<td>" + obj.F[i].goals + "</td>");
		tr.append("<td>" + obj.F[i].goalsAgainst + "</td>");
		tr.append("<td>" + obj.F[i].goalDifference + "</td>");
		$('#tableF').append(tr);
	}

}); 
