<?php header('Location: /index.html') ; 

if (isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) == 'on') {
  $base_url = 'https://goal17.herokuapp.com';
}
else {
  $base_url = 'http://goal17.herokuapp.com';
}

?>