function check(f)
{
	var x = f.gameResult;
	
	var y = x.value;
	
	if(y == 'france'){
	$.ajax({
	headers: { 'X-Auth-Token': 'e03efd7c7860409290d19f33137385ef' },
	url: 'http://api.football-data.org/v1/teams/773/fixtures',
	dataType: 'json',
	type: 'GET',
	}).done(function(response) {	
		
		$('#tableallresult tr').not(function(){ return !!$(this).has('th').length; }).remove();
		
		for(var i = 0; i < response.fixtures.length; i++) {
				
		var date = response.fixtures[i].date;
		
		var result = response.fixtures[i].result;
		
		var firstHalfScoreHome = result.goalsHomeTeam;
		var firstHalfScoreAway = result.goalsAwayTeam;
		
		var secondResult = result.halfTime;
		
		var secondHalfScoreHome = secondResult.goalsHomeTeam;
		var secondHalfScoreAway = secondResult.goalsAwayTeam;
		
		if(result.extraTime == null)
		{
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		else
		{
			var extraResult = result.extraTime;
			
			var extraScoreHome = extraResult.goalsHomeTeam;
			var extraScoreAway = extraResult.goalsAwayTeam;
			
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome + extraScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway + extraScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		}
	}); 
	}
	else if(y =='romania'){
	$.ajax({
	headers: { 'X-Auth-Token': 'e03efd7c7860409290d19f33137385ef' },
	url: 'http://api.football-data.org/v1/teams/811/fixtures',
	dataType: 'json',
	type: 'GET',
	}).done(function(response) {
 
		$('#tableallresult tr').not(function(){ return !!$(this).has('th').length; }).remove();
		
		for(var i = 0; i < response.fixtures.length; i++) {
		
		var date = response.fixtures[i].date;
		
		var result = response.fixtures[i].result;
		
		var firstHalfScoreHome = result.goalsHomeTeam;
		var firstHalfScoreAway = result.goalsAwayTeam;
		
		var secondResult = result.halfTime;
		
		var secondHalfScoreHome = secondResult.goalsHomeTeam;
		var secondHalfScoreAway = secondResult.goalsAwayTeam;
		
		if(result.extraTime == null)
		{
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		else
		{
			var extraResult = result.extraTime;
			
			var extraScoreHome = extraResult.goalsHomeTeam;
			var extraScoreAway = extraResult.goalsAwayTeam;
			
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome + extraScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway + extraScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		}
	}); 
	}
	else if(y =='albania'){
	$.ajax({
	headers: { 'X-Auth-Token': 'e03efd7c7860409290d19f33137385ef' },
	url: 'http://api.football-data.org/v1/teams/1065/fixtures',
	dataType: 'json',
	type: 'GET',
	}).done(function(response) {
		
		$('#tableallresult tr').not(function(){ return !!$(this).has('th').length; }).remove();
 
		for(var i = 0; i < response.fixtures.length; i++) {
		
		var date = response.fixtures[i].date;
		
		var result = response.fixtures[i].result;
		
		var firstHalfScoreHome = result.goalsHomeTeam;
		var firstHalfScoreAway = result.goalsAwayTeam;
		
		var secondResult = result.halfTime;
		
		var secondHalfScoreHome = secondResult.goalsHomeTeam;
		var secondHalfScoreAway = secondResult.goalsAwayTeam;
		
		if(result.extraTime == null)
		{
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		else
		{
			var extraResult = result.extraTime;
			
			var extraScoreHome = extraResult.goalsHomeTeam;
			var extraScoreAway = extraResult.goalsAwayTeam;
			
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome + extraScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway + extraScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		}
	}); 
	}
	else if(y =='switzerland'){
	$.ajax({
	headers: { 'X-Auth-Token': 'e03efd7c7860409290d19f33137385ef' },
	url: 'http://api.football-data.org/v1/teams/788/fixtures',
	dataType: 'json',
	type: 'GET',
	}).done(function(response) {
		
		$('#tableallresult tr').not(function(){ return !!$(this).has('th').length; }).remove();
 
		for(var i = 0; i < response.fixtures.length; i++) {
		
		var date = response.fixtures[i].date;
		
		var result = response.fixtures[i].result;
		
		var firstHalfScoreHome = result.goalsHomeTeam;
		var firstHalfScoreAway = result.goalsAwayTeam;
		
		var secondResult = result.halfTime;
		
		var secondHalfScoreHome = secondResult.goalsHomeTeam;
		var secondHalfScoreAway = secondResult.goalsAwayTeam;
		
		if(result.extraTime == null)
		{
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		else
		{
			var extraResult = result.extraTime;
			
			var extraScoreHome = extraResult.goalsHomeTeam;
			var extraScoreAway = extraResult.goalsAwayTeam;
			
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome + extraScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway + extraScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		}
	}); 
	}
	else if(y =='wales'){
	$.ajax({
	headers: { 'X-Auth-Token': 'e03efd7c7860409290d19f33137385ef' },
	url: 'http://api.football-data.org/v1/teams/833/fixtures',
	dataType: 'json',
	type: 'GET',
	}).done(function(response) {
		
		$('#tableallresult tr').not(function(){ return !!$(this).has('th').length; }).remove();
 
		for(var i = 0; i < response.fixtures.length; i++) {
		
		var date = response.fixtures[i].date;
		
		var result = response.fixtures[i].result;
		
		var firstHalfScoreHome = result.goalsHomeTeam;
		var firstHalfScoreAway = result.goalsAwayTeam;
		
		var secondResult = result.halfTime;
		
		var secondHalfScoreHome = secondResult.goalsHomeTeam;
		var secondHalfScoreAway = secondResult.goalsAwayTeam;
		
		if(result.extraTime == null)
		{
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		else
		{
			var extraResult = result.extraTime;
			
			var extraScoreHome = extraResult.goalsHomeTeam;
			var extraScoreAway = extraResult.goalsAwayTeam;
			
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome + extraScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway + extraScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		}
	}); 
	}
	else if(y =='slovakia'){
	$.ajax({
	headers: { 'X-Auth-Token': 'e03efd7c7860409290d19f33137385ef' },
	url: 'http://api.football-data.org/v1/teams/768/fixtures',
	dataType: 'json',
	type: 'GET',
	}).done(function(response) {
		
		$('#tableallresult tr').not(function(){ return !!$(this).has('th').length; }).remove();
 
		for(var i = 0; i < response.fixtures.length; i++) {
		
		var date = response.fixtures[i].date;
		
		var result = response.fixtures[i].result;
		
		var firstHalfScoreHome = result.goalsHomeTeam;
		var firstHalfScoreAway = result.goalsAwayTeam;
		
		var secondResult = result.halfTime;
		
		var secondHalfScoreHome = secondResult.goalsHomeTeam;
		var secondHalfScoreAway = secondResult.goalsAwayTeam;
		
		if(result.extraTime == null)
		{
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		else
		{
			var extraResult = result.extraTime;
			
			var extraScoreHome = extraResult.goalsHomeTeam;
			var extraScoreAway = extraResult.goalsAwayTeam;
			
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome + extraScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway + extraScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		}
	}); 
	}
	else if(y =='england'){
	$.ajax({
	headers: { 'X-Auth-Token': 'e03efd7c7860409290d19f33137385ef' },
	url: 'http://api.football-data.org/v1/teams/770/fixtures',
	dataType: 'json',
	type: 'GET',
	}).done(function(response) {
		
		$('#tableallresult tr').not(function(){ return !!$(this).has('th').length; }).remove();
 
		for(var i = 0; i < response.fixtures.length; i++) {
		
		var date = response.fixtures[i].date;
		
		var result = response.fixtures[i].result;
		
		var firstHalfScoreHome = result.goalsHomeTeam;
		var firstHalfScoreAway = result.goalsAwayTeam;
		
		var secondResult = result.halfTime;
		
		var secondHalfScoreHome = secondResult.goalsHomeTeam;
		var secondHalfScoreAway = secondResult.goalsAwayTeam;
		
		if(result.extraTime == null)
		{
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		else
		{
			var extraResult = result.extraTime;
			
			var extraScoreHome = extraResult.goalsHomeTeam;
			var extraScoreAway = extraResult.goalsAwayTeam;
			
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome + extraScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway + extraScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		}
	}); 
	}
	else if(y =='russia'){
	$.ajax({
	headers: { 'X-Auth-Token': 'e03efd7c7860409290d19f33137385ef' },
	url: 'http://api.football-data.org/v1/teams/808/fixtures',
	dataType: 'json',
	type: 'GET',
	}).done(function(response) {
		
		$('#tableallresult tr').not(function(){ return !!$(this).has('th').length; }).remove();
 
		for(var i = 0; i < response.fixtures.length; i++) {
		
		var date = response.fixtures[i].date;
		
		var result = response.fixtures[i].result;
		
		var firstHalfScoreHome = result.goalsHomeTeam;
		var firstHalfScoreAway = result.goalsAwayTeam;
		
		var secondResult = result.halfTime;
		
		var secondHalfScoreHome = secondResult.goalsHomeTeam;
		var secondHalfScoreAway = secondResult.goalsAwayTeam;
		
		if(result.extraTime == null)
		{
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		else
		{
			var extraResult = result.extraTime;
			
			var extraScoreHome = extraResult.goalsHomeTeam;
			var extraScoreAway = extraResult.goalsAwayTeam;
			
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome + extraScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway + extraScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		}
	}); 
	}
	else if(y =='turkey'){
	$.ajax({
	headers: { 'X-Auth-Token': 'e03efd7c7860409290d19f33137385ef' },
	url: 'http://api.football-data.org/v1/teams/803/fixtures',
	dataType: 'json',
	type: 'GET',
	}).done(function(response) {
		
		$('#tableallresult tr').not(function(){ return !!$(this).has('th').length; }).remove();
 
		for(var i = 0; i < response.fixtures.length; i++) {
		
		var date = response.fixtures[i].date;
		
		var result = response.fixtures[i].result;
		
		var firstHalfScoreHome = result.goalsHomeTeam;
		var firstHalfScoreAway = result.goalsAwayTeam;
		
		var secondResult = result.halfTime;
		
		var secondHalfScoreHome = secondResult.goalsHomeTeam;
		var secondHalfScoreAway = secondResult.goalsAwayTeam;
		
		if(result.extraTime == null)
		{
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		else
		{
			var extraResult = result.extraTime;
			
			var extraScoreHome = extraResult.goalsHomeTeam;
			var extraScoreAway = extraResult.goalsAwayTeam;
			
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome + extraScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway + extraScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		}
	}); 
	}
	else if(y =='croatia'){
	$.ajax({
	headers: { 'X-Auth-Token': 'e03efd7c7860409290d19f33137385ef' },
	url: 'http://api.football-data.org/v1/teams/799/fixtures',
	dataType: 'json',
	type: 'GET',
	}).done(function(response) {
		
		$('#tableallresult tr').not(function(){ return !!$(this).has('th').length; }).remove();
 
		for(var i = 0; i < response.fixtures.length; i++) {
		
		var date = response.fixtures[i].date;
		
		var result = response.fixtures[i].result;
		
		var firstHalfScoreHome = result.goalsHomeTeam;
		var firstHalfScoreAway = result.goalsAwayTeam;
		
		var secondResult = result.halfTime;
		
		var secondHalfScoreHome = secondResult.goalsHomeTeam;
		var secondHalfScoreAway = secondResult.goalsAwayTeam;
		
		if(result.extraTime == null)
		{
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		else
		{
			var extraResult = result.extraTime;
			
			var extraScoreHome = extraResult.goalsHomeTeam;
			var extraScoreAway = extraResult.goalsAwayTeam;
			
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome + extraScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway + extraScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		}
	}); 
	}
	else if(y =='poland'){
	$.ajax({
	headers: { 'X-Auth-Token': 'e03efd7c7860409290d19f33137385ef' },
	url: 'http://api.football-data.org/v1/teams/794/fixtures',
	dataType: 'json',
	type: 'GET',
	}).done(function(response) {
		
		$('#tableallresult tr').not(function(){ return !!$(this).has('th').length; }).remove();
 
		for(var i = 0; i < response.fixtures.length; i++) {
		
		var date = response.fixtures[i].date;
		
		var result = response.fixtures[i].result;
		
		var firstHalfScoreHome = result.goalsHomeTeam;
		var firstHalfScoreAway = result.goalsAwayTeam;
		
		var secondResult = result.halfTime;
		
		var secondHalfScoreHome = secondResult.goalsHomeTeam;
		var secondHalfScoreAway = secondResult.goalsAwayTeam;
		
		if(result.extraTime == null)
		{
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		else
		{
			var extraResult = result.extraTime;
			
			var extraScoreHome = extraResult.goalsHomeTeam;
			var extraScoreAway = extraResult.goalsAwayTeam;
			
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome + extraScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway + extraScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		}
	}); 
	}
	else if(y =='ireland'){
	$.ajax({
	headers: { 'X-Auth-Token': 'e03efd7c7860409290d19f33137385ef' },
	url: 'http://api.football-data.org/v1/teams/829/fixtures',
	dataType: 'json',
	type: 'GET',
	}).done(function(response) {
		
		$('#tableallresult tr').not(function(){ return !!$(this).has('th').length; }).remove();
 
		for(var i = 0; i < response.fixtures.length; i++) {
		
		var date = response.fixtures[i].date;
		
		var result = response.fixtures[i].result;
		
		var firstHalfScoreHome = result.goalsHomeTeam;
		var firstHalfScoreAway = result.goalsAwayTeam;
		
		var secondResult = result.halfTime;
		
		var secondHalfScoreHome = secondResult.goalsHomeTeam;
		var secondHalfScoreAway = secondResult.goalsAwayTeam;
		
		if(result.extraTime == null)
		{
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		else
		{
			var extraResult = result.extraTime;
			
			var extraScoreHome = extraResult.goalsHomeTeam;
			var extraScoreAway = extraResult.goalsAwayTeam;
			
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome + extraScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway + extraScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		}
	}); 
	}
	else if(y =='germany'){
	$.ajax({
	headers: { 'X-Auth-Token': 'e03efd7c7860409290d19f33137385ef' },
	url: 'http://api.football-data.org/v1/teams/759/fixtures',
	dataType: 'json',
	type: 'GET',
	}).done(function(response) {
		
		$('#tableallresult tr').not(function(){ return !!$(this).has('th').length; }).remove();
 
		for(var i = 0; i < response.fixtures.length; i++) {
		
		var date = response.fixtures[i].date;
		
		var result = response.fixtures[i].result;
		
		var firstHalfScoreHome = result.goalsHomeTeam;
		var firstHalfScoreAway = result.goalsAwayTeam;
		
		var secondResult = result.halfTime;
		
		var secondHalfScoreHome = secondResult.goalsHomeTeam;
		var secondHalfScoreAway = secondResult.goalsAwayTeam;
		
		if(result.extraTime == null)
		{
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		else
		{
			var extraResult = result.extraTime;
			
			var extraScoreHome = extraResult.goalsHomeTeam;
			var extraScoreAway = extraResult.goalsAwayTeam;
			
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome + extraScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway + extraScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		}
	}); 
	}
	else if(y =='ukraine'){
	$.ajax({
	headers: { 'X-Auth-Token': 'e03efd7c7860409290d19f33137385ef' },
	url: 'http://api.football-data.org/v1/teams/790/fixtures',
	dataType: 'json',
	type: 'GET',
	}).done(function(response) {
		
		$('#tableallresult tr').not(function(){ return !!$(this).has('th').length; }).remove();
 
		for(var i = 0; i < response.fixtures.length; i++) {
		
		var date = response.fixtures[i].date;
		
		var result = response.fixtures[i].result;
		
		var firstHalfScoreHome = result.goalsHomeTeam;
		var firstHalfScoreAway = result.goalsAwayTeam;
		
		var secondResult = result.halfTime;
		
		var secondHalfScoreHome = secondResult.goalsHomeTeam;
		var secondHalfScoreAway = secondResult.goalsAwayTeam;
		
		if(result.extraTime == null)
		{
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		else
		{
			var extraResult = result.extraTime;
			
			var extraScoreHome = extraResult.goalsHomeTeam;
			var extraScoreAway = extraResult.goalsAwayTeam;
			
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome + extraScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway + extraScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		}
	}); 
	}
	else if(y =='spain'){
	$.ajax({
	headers: { 'X-Auth-Token': 'e03efd7c7860409290d19f33137385ef' },
	url: 'http://api.football-data.org/v1/teams/760/fixtures',
	dataType: 'json',
	type: 'GET',
	}).done(function(response) {
		
		$('#tableallresult tr').not(function(){ return !!$(this).has('th').length; }).remove();
 
		for(var i = 0; i < response.fixtures.length; i++) {
		
		var date = response.fixtures[i].date;
		
		var result = response.fixtures[i].result;
		
		var firstHalfScoreHome = result.goalsHomeTeam;
		var firstHalfScoreAway = result.goalsAwayTeam;
		
		var secondResult = result.halfTime;
		
		var secondHalfScoreHome = secondResult.goalsHomeTeam;
		var secondHalfScoreAway = secondResult.goalsAwayTeam;
		
		if(result.extraTime == null)
		{
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		else
		{
			var extraResult = result.extraTime;
			
			var extraScoreHome = extraResult.goalsHomeTeam;
			var extraScoreAway = extraResult.goalsAwayTeam;
			
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome + extraScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway + extraScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		}
	}); 
	}
	else if(y =='czech'){
	$.ajax({
	headers: { 'X-Auth-Token': 'e03efd7c7860409290d19f33137385ef' },
	url: 'http://api.football-data.org/v1/teams/798/fixtures',
	dataType: 'json',
	type: 'GET',
	}).done(function(response) {
		
		$('#tableallresult tr').not(function(){ return !!$(this).has('th').length; }).remove();
 
		for(var i = 0; i < response.fixtures.length; i++) {
		
		var date = response.fixtures[i].date;
		
		var result = response.fixtures[i].result;
		
		var firstHalfScoreHome = result.goalsHomeTeam;
		var firstHalfScoreAway = result.goalsAwayTeam;
		
		var secondResult = result.halfTime;
		
		var secondHalfScoreHome = secondResult.goalsHomeTeam;
		var secondHalfScoreAway = secondResult.goalsAwayTeam;
		
		if(result.extraTime == null)
		{
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		else
		{
			var extraResult = result.extraTime;
			
			var extraScoreHome = extraResult.goalsHomeTeam;
			var extraScoreAway = extraResult.goalsAwayTeam;
			
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome + extraScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway + extraScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		}
	}); 
	}
	else if(y =='republic'){
	$.ajax({
	headers: { 'X-Auth-Token': 'e03efd7c7860409290d19f33137385ef' },
	url: 'http://api.football-data.org/v1/teams/806/fixtures',
	dataType: 'json',
	type: 'GET',
	}).done(function(response) {
		
		$('#tableallresult tr').not(function(){ return !!$(this).has('th').length; }).remove();
 
		for(var i = 0; i < response.fixtures.length; i++) {
		
		var date = response.fixtures[i].date;
		
		var result = response.fixtures[i].result;
		
		var firstHalfScoreHome = result.goalsHomeTeam;
		var firstHalfScoreAway = result.goalsAwayTeam;
		
		var secondResult = result.halfTime;
		
		var secondHalfScoreHome = secondResult.goalsHomeTeam;
		var secondHalfScoreAway = secondResult.goalsAwayTeam;
		
		if(result.extraTime == null)
		{
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		else
		{
			var extraResult = result.extraTime;
			
			var extraScoreHome = extraResult.goalsHomeTeam;
			var extraScoreAway = extraResult.goalsAwayTeam;
			
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome + extraScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway + extraScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		}
	}); 
	}
	else if(y =='sweden'){
	$.ajax({
	headers: { 'X-Auth-Token': 'e03efd7c7860409290d19f33137385ef' },
	url: 'http://api.football-data.org/v1/teams/792/fixtures',
	dataType: 'json',
	type: 'GET',
	}).done(function(response) {
		
		$('#tableallresult tr').not(function(){ return !!$(this).has('th').length; }).remove();
 
		for(var i = 0; i < response.fixtures.length; i++) {
		
		var date = response.fixtures[i].date;
		
		var result = response.fixtures[i].result;
		
		var firstHalfScoreHome = result.goalsHomeTeam;
		var firstHalfScoreAway = result.goalsAwayTeam;
		
		var secondResult = result.halfTime;
		
		var secondHalfScoreHome = secondResult.goalsHomeTeam;
		var secondHalfScoreAway = secondResult.goalsAwayTeam;
		
		if(result.extraTime == null)
		{
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		else
		{
			var extraResult = result.extraTime;
			
			var extraScoreHome = extraResult.goalsHomeTeam;
			var extraScoreAway = extraResult.goalsAwayTeam;
			
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome + extraScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway + extraScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		}
	}); 
	}
	else if(y =='belgium'){
	$.ajax({
	headers: { 'X-Auth-Token': 'e03efd7c7860409290d19f33137385ef' },
	url: 'http://api.football-data.org/v1/teams/805/fixtures',
	dataType: 'json',
	type: 'GET',
	}).done(function(response) {
		
		$('#tableallresult tr').not(function(){ return !!$(this).has('th').length; }).remove();
 
		for(var i = 0; i < response.fixtures.length; i++) {
		
		var date = response.fixtures[i].date;
		
		var result = response.fixtures[i].result;
		
		var firstHalfScoreHome = result.goalsHomeTeam;
		var firstHalfScoreAway = result.goalsAwayTeam;
		
		var secondResult = result.halfTime;
		
		var secondHalfScoreHome = secondResult.goalsHomeTeam;
		var secondHalfScoreAway = secondResult.goalsAwayTeam;
		
		if(result.extraTime == null)
		{
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		else
		{
			var extraResult = result.extraTime;
			
			var extraScoreHome = extraResult.goalsHomeTeam;
			var extraScoreAway = extraResult.goalsAwayTeam;
			
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome + extraScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway + extraScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		}
	}); 
	}
	else if(y =='italy'){
	$.ajax({
	headers: { 'X-Auth-Token': 'e03efd7c7860409290d19f33137385ef' },
	url: 'http://api.football-data.org/v1/teams/784/fixtures',
	dataType: 'json',
	type: 'GET',
	}).done(function(response) {
		
		$('#tableallresult tr').not(function(){ return !!$(this).has('th').length; }).remove();
 
		for(var i = 0; i < response.fixtures.length; i++) {
		
		var date = response.fixtures[i].date;
		
		var result = response.fixtures[i].result;
		
		var firstHalfScoreHome = result.goalsHomeTeam;
		var firstHalfScoreAway = result.goalsAwayTeam;
		
		var secondResult = result.halfTime;
		
		var secondHalfScoreHome = secondResult.goalsHomeTeam;
		var secondHalfScoreAway = secondResult.goalsAwayTeam;
		
		if(result.extraTime == null)
		{
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		else
		{
			var extraResult = result.extraTime;
			
			var extraScoreHome = extraResult.goalsHomeTeam;
			var extraScoreAway = extraResult.goalsAwayTeam;
			
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome + extraScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway + extraScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		}
	}); 
	}
	else if(y =='austria'){
	$.ajax({
	headers: { 'X-Auth-Token': 'e03efd7c7860409290d19f33137385ef' },
	url: 'http://api.football-data.org/v1/teams/816/fixtures',
	dataType: 'json',
	type: 'GET',
	}).done(function(response) {
		
		$('#tableallresult tr').not(function(){ return !!$(this).has('th').length; }).remove();
 
		for(var i = 0; i < response.fixtures.length; i++) {
		
		var date = response.fixtures[i].date;
		
		var result = response.fixtures[i].result;
		
		var firstHalfScoreHome = result.goalsHomeTeam;
		var firstHalfScoreAway = result.goalsAwayTeam;
		
		var secondResult = result.halfTime;
		
		var secondHalfScoreHome = secondResult.goalsHomeTeam;
		var secondHalfScoreAway = secondResult.goalsAwayTeam;
		
		if(result.extraTime == null)
		{
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		else
		{
			var extraResult = result.extraTime;
			
			var extraScoreHome = extraResult.goalsHomeTeam;
			var extraScoreAway = extraResult.goalsAwayTeam;
			
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome + extraScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway + extraScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		}
	}); 
	}
	else if(y =='hungary'){
	$.ajax({
	headers: { 'X-Auth-Token': 'e03efd7c7860409290d19f33137385ef' },
	url: 'http://api.football-data.org/v1/teams/827/fixtures',
	dataType: 'json',
	type: 'GET',
	}).done(function(response) {
		
		$('#tableallresult tr').not(function(){ return !!$(this).has('th').length; }).remove();
 
		for(var i = 0; i < response.fixtures.length; i++) {
		
		var date = response.fixtures[i].date;
		
		var result = response.fixtures[i].result;
		
		var firstHalfScoreHome = result.goalsHomeTeam;
		var firstHalfScoreAway = result.goalsAwayTeam;
		
		var secondResult = result.halfTime;
		
		var secondHalfScoreHome = secondResult.goalsHomeTeam;
		var secondHalfScoreAway = secondResult.goalsAwayTeam;
		
		if(result.extraTime == null)
		{
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		else
		{
			var extraResult = result.extraTime;
			
			var extraScoreHome = extraResult.goalsHomeTeam;
			var extraScoreAway = extraResult.goalsAwayTeam;
			
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome + extraScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway + extraScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		}
	}); 
	}
	else if(y =='portugal'){
	$.ajax({
	headers: { 'X-Auth-Token': 'e03efd7c7860409290d19f33137385ef' },
	url: 'http://api.football-data.org/v1/teams/765/fixtures',
	dataType: 'json',
	type: 'GET',
	}).done(function(response) {
		
		$('#tableallresult tr').not(function(){ return !!$(this).has('th').length; }).remove();
 
		for(var i = 0; i < response.fixtures.length; i++) {
		
		var date = response.fixtures[i].date;
		
		var result = response.fixtures[i].result;
		
		var firstHalfScoreHome = result.goalsHomeTeam;
		var firstHalfScoreAway = result.goalsAwayTeam;
		
		var secondResult = result.halfTime;
		
		var secondHalfScoreHome = secondResult.goalsHomeTeam;
		var secondHalfScoreAway = secondResult.goalsAwayTeam;
		
		if(result.extraTime == null)
		{
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		else
		{
			var extraResult = result.extraTime;
			
			var extraScoreHome = extraResult.goalsHomeTeam;
			var extraScoreAway = extraResult.goalsAwayTeam;
			
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome + extraScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway + extraScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		}
	}); 
	}
	else if(y =='iceland'){
	$.ajax({
	headers: { 'X-Auth-Token': 'e03efd7c7860409290d19f33137385ef' },
	url: 'http://api.football-data.org/v1/teams/1066/fixtures',
	dataType: 'json',
	type: 'GET',
	}).done(function(response) {
		
		$('#tableallresult tr').not(function(){ return !!$(this).has('th').length; }).remove();
 
 
		for(var i = 0; i < response.fixtures.length; i++) {
		
		var date = response.fixtures[i].date;
		
		var result = response.fixtures[i].result;
		
		var firstHalfScoreHome = result.goalsHomeTeam;
		var firstHalfScoreAway = result.goalsAwayTeam;
		
		var secondResult = result.halfTime;
		
		var secondHalfScoreHome = secondResult.goalsHomeTeam;
		var secondHalfScoreAway = secondResult.goalsAwayTeam;
		
		if(result.extraTime == null)
		{
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		else
		{
			var extraResult = result.extraTime;
			
			var extraScoreHome = extraResult.goalsHomeTeam;
			var extraScoreAway = extraResult.goalsAwayTeam;
			
			var totalHomeScore = firstHalfScoreHome + secondHalfScoreHome + extraScoreHome;
			var totalAwayScore = firstHalfScoreAway + secondHalfScoreAway + extraScoreAway;
			
			tr = $('<tr/>');
			tr.append("<td>" + date + "</td>");
			tr.append("<td>" + response.fixtures[i].homeTeamName + "</td>");			
			tr.append("<td>" + totalHomeScore + " - " + totalAwayScore + "</td>");
			tr.append("<td>" + response.fixtures[i].awayTeamName + "</td>");
			$('#tableallresult').append(tr);
		}
		}
	}); 
	}
			
}

